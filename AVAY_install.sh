#!/bin/bash
# Copyright (c) 2022, Avay-BioSciences https://www.avay.tech/  

echo ""
echo "[Note] This Bash file is Confidential and used by Avay BioScience Pvt. Ltd."
echo " Make sure Internet is Connected During the time this Script is running"
echo " "
echo ""
echo " PRESS [ENTER] TO CONTINUE THE INSTALLATION "
echo " IF YOU WANT TO CANCEL, PRESS [CTRL] + [C] "
read
echo 
echo
#
echo "                  Removing Preinstalled Games                 "
echo
sudo apt remove -y --purge code-the-classics
sudo apt remove -y --purge sonic-pi scratch python3-thonny wolfram wolfram-engine libreoffice

sudo apt -y clean -y
sudo apt -y autoremove

echo 
echo
echo "                 Updating and Upgrading System                " 
echo 

sudo apt -y update && sudo apt -y upgrade
sudo apt install git
sudo apt -y clean -y
sudo apt -y autoremove
#
echo 
echo
echo "                 Installing  and Upgrading Python3 Language                  "
sudo apt-get install idle3 -y
sudo apt install python3-pip -y

echo
echo 
echo
echo "[Installing Python3 Packages]"
# uninstall serial  library  and install pyserial

pip3 install pyserial
pip3 uninstall serial 

pip3 install psutil
pip3 install GitPython
#pip3 install pyside2 
sudo apt-get -y install python3-pyside2.qt3dcore python3-pyside2.qt3dinput python3-pyside2.qt3dlogic python3-pyside2.qt3drender python3-pyside2.qtcharts python3-pyside2.qtconcurrent python3-pyside2.qtcore python3-pyside2.qtgui python3-pyside2.qthelp python3-pyside2.qtlocation python3-pyside2.qtmultimedia python3-pyside2.qtmultimediawidgets python3-pyside2.qtnetwork python3-pyside2.qtopengl python3-pyside2.qtpositioning python3-pyside2.qtprintsupport python3-pyside2.qtqml python3-pyside2.qtquick python3-pyside2.qtquickwidgets python3-pyside2.qtscript python3-pyside2.qtscripttools python3-pyside2.qtsensors python3-pyside2.qtsql python3-pyside2.qtsvg python3-pyside2.qttest python3-pyside2.qttexttospeech python3-pyside2.qtuitools python3-pyside2.qtwebchannel python3-pyside2.qtwebsockets python3-pyside2.qtwidgets python3-pyside2.qtx11extras python3-pyside2.qtxml python3-pyside2.qtxmlpatterns 
pip3 install pyside2
pip3 install cython


sudo apt -y clean -y
sudo apt -y autoremove

git clone https://gitlab.com/jatin.avay/mito-basic/
cd test 
ls
export XDG_RUNTIME_DIR=/home/pi/test
export RUNLEVEL=3
source ~/.bashrc 

#echo "sudo python3 /home/pi/test/main.py & " >> /etc/profile

# sudo nano /etc/xdg/lxsession/LXDE-pi/autostart  # file containing autostart config
#@/usr/bin/python3 /home/pi/test/main.py


echo "@/usr/bin/python3 /home/pi/mito-basic/main.py " >> /etc/xdg/lxsession/LXDE-pi/autostart

echo
echo
echo "                		    Installing  Network-Manager                 "
echo
sudo apt install -y network-manager network-manager-gnome
sudo systemctl enable NetworkManager
sudo systemctl restart NetworkManager

#
echo
echo
echo "                                      Complete!!!                                     "
echo  
echo
echo " PRESS [ENTER] TO REBOOT SYSTEM TO TAKE EFFECTS "
echo " IF YOU WANT TO CANCEL, PRESS [CTRL] + [C] "
read
echo
echo
echo "		R 	E	 E 	B	 O	 O	 T	 I	 N	 G "
read
sudo reboot

exit 0
