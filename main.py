import os

if "XDG_RUNTIME_DIR" not in os.environ:
    os.environ["XDG_RUNTIME_DIR"] = os.getcwd()
    
from Libs.Main  import main
main()
